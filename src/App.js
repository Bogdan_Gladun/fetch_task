import React, { Component } from "react";
import ApiService from "./services/api";
import List from "./list/list";

class App extends Component {
  api = new ApiService();

  state = {
    seacrhString: "",
    hits: [],
    selectedItem: null
  };

  handleKeyDown = e => {
    if (e.key === "Escape") {
      this.setState(({ seacrhString, hits }) => {
        return {
          seacrhString: "",
          hits: []
        };
      });
    } else if (e.key === "Enter") {
      this.setState(({ selectedItem }) => {
        return {
          selectedItem: 0
        };
      });
    } else if (e.key === "Backspace") {
      this.setState(({ selectedItem }) => {
        return {
          selectedItem: null
        };
      });
    }
  };

  handleFindIndex = (id) => {
    this.setState(({selectedItem}) => {
      return {
        selectedItem: id
      }
    })
  }

  findCountry = countryName => {
    this.api
      .getCountry(countryName)
      .then(res => {
        this.setState({
          hits: res.hits.slice(0, 5).map(country => {
            return {
              locale_names: country.locale_names[0],
              population: country.population
            };
          })
        });
      })
      .catch(e => console.log(e));
  };

  matchHits = e => {
    if (e.target.value.length === 0) {
      this.setState({
        hits: []
      });
    }
    this.setState(
      {
        seacrhString: e.target.value
      },
      () =>
        this.state.seacrhString
          ? this.findCountry(this.state.seacrhString)
          : null
    );
  };

  render() {
    const { hits, selectedItem } = this.state;
    console.log(this.state);
    const filteredInfo = this.state.hits.length ? (
      <List labels={this.state.hits}
      handleFindIndex={this.handleFindIndex} />
    ) : (
      <h3>No results...</h3>
    );
    return (
      <>
        <input
          type="text"
          placeholder="type to search"
          onChange={this.matchHits}
          value={this.state.seacrhString}
          onKeyDown={this.handleKeyDown}
        />
        {filteredInfo}
        <div>{selectedItem !== null && hits.length ? hits[selectedItem].population : null}</div>
      </>
    );
  }
}

export default App;
