import React, { Component } from "react";

import "./list-item.css";

export default class List extends Component {
  state = {
    clickedItem: 0
  };

  selectID = id => {
    this.setState({
      clickedItem: id
    });
  };

  render() {
    const notActive = "list-item";
    const active = "list-item__active";
    const { handleFindIndex } = this.props;
    const elements = this.props.labels.map((el, idx) => {
      return (
        <li
          className={idx === this.state.clickedItem ? active : notActive}
          key={idx}
          onMouseMove={() => {
            this.selectID(idx);
          }}
          onClick={() => handleFindIndex(idx)}
        >
          {el.locale_names}
        </li>
      );
    });

    return <ul>{elements}</ul>;
  }
}
