export default class ApiService {

     getCountry = async (string = "") => {
        const res = await fetch("https://places-dsn.algolia.net/1/places/query", {
            method: 'POST',
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify({query: string,
                                    language: "en",
                                    type: "country"})
          })
         const body = await res.json();
         console.log(body)
         return body;    
    }
}



